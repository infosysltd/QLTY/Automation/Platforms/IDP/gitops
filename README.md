# GitOps for [onlineboutique](https://gitlab.com/infosysltd/QLTY/Automation/Platforms/IDP/IDP)

## Architecture
![fluxcd arcitecture](images/fluxcd.png)

## Bootstrap [fluxcd](https://fluxcd.io/) 
```bash
export GITLAB_TOKEN=<user-gitlab-api-access-token>

flux bootstrap gitlab \
--deploy-token-auth \
--owner=infosysltd/QLTY/Automation/Platforms/IDP \
--repository=gitops \    
--branch=main \  
--path=clusters/rancherdesktop-cluster

kubectl get all -n flux-system 
NAME                                           READY   STATUS    RESTARTS   AGE
pod/kustomize-controller-7b7b47f459-bnkpf      1/1     Running   0          9m23s
pod/helm-controller-5d8d5fc6fd-fh4p9           1/1     Running   0          9m23s
pod/notification-controller-5bb6647999-86cxf   1/1     Running   0          9m23s
pod/source-controller-7667765cd7-7t87g         1/1     Running   0          9m23s

NAME                              TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/notification-controller   ClusterIP   10.43.229.42    <none>        80/TCP    9m23s
service/source-controller         ClusterIP   10.43.105.210   <none>        80/TCP    9m23s
service/webhook-receiver          ClusterIP   10.43.245.157   <none>        80/TCP    9m23s

NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/helm-controller           1/1     1            1           9m23s
deployment.apps/kustomize-controller      1/1     1            1           9m23s
deployment.apps/notification-controller   1/1     1            1           9m23s
deployment.apps/source-controller         1/1     1            1           9m23s

NAME                                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/kustomize-controller-7b7b47f459      1         1         1       9m23s
replicaset.apps/helm-controller-5d8d5fc6fd           1         1         1       9m23s
replicaset.apps/notification-controller-5bb6647999   1         1         1       9m23s
replicaset.apps/source-controller-7667765cd7         1         1         1       9m23s

kubectl -n flux-system get secrets                     
NAME          TYPE     DATA   AGE
flux-system   Opaque   2      9m45s


```
## Create `idp-repo-token` image pull secret

```bash
kubectl create secret docker-registry idp-repo-token --docker-server=registry.gitlab.com/infosysltd/qlty/automation/platforms/idp/idp --docker-username=raghavendramallela --docker-password=<idp-oci-token> --docker-email=mrgdevops@gmail.com

```
## Create helm oci repo, secret (for repo auth), & helm release CRDs at `clusters/<cluster-name>` path

```bash
bat ./clusters/rancherdesktop-cluster/idp-helm-oci-repository.yaml
```
```yaml
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: HelmRepository
metadata:
  name: idp-helm-oci-repo
  namespace: default
spec:clusters/rancherdesktop-cluster/idp-helm-oci-repository.yaml
  type: oci
  interval: 1m0s
  url: oci://registry.gitlab.com/infosysltd/qlty/automation/platforms/idp/idp/helm-charts
  secretRef:
    name: idp-helm-oci-repo-secret

---
apiVersion: v1
kind: Secret
metadata:
  name: idp-helm-oci-repo-secret
  namespace: default
stringData:
  username: raghavendramallela
  password: "glpat-7tUTdbmDNns6ASKMEhMu"

---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: onlineboutique-release
  namespace: default
spec:
  chart:
    spec:
      chart: onlineboutique
      sourceRef:
        kind: HelmRepository
        name: idp-helm-oci-repo
        namespace: default
  interval: 1m0s
```

## Check helm release
```bash
helm ls                         
NAME                    NAMESPACE       REVISION        UPDATED                                 STATUS  CHART                   APP VERSION
onlineboutique-release  default         1               2024-03-09 14:13:04.187856556 +0000 UTC deployed  onlineboutique-0.9.0    v0.9.0    

kubectl get all -n default                                       
NAME                                         READY   STATUS    RESTARTS        AGE
pod/redis-cart-696448dd5c-mjqwf              1/1     Running   0               26m
pod/shippingservice-6dc8984595-r5hvz         1/1     Running   0               26m
pod/productcatalogservice-76d9484687-5t995   1/1     Running   0               26m
pod/frontend-5f946c96fc-hk2d7                1/1     Running   0               26m
pod/paymentservice-6d65f6bff5-hmwzw          1/1     Running   0               26m
pod/checkoutservice-5f4f746c97-kbp6x         1/1     Running   0               26m
pod/currencyservice-795fc646df-4cdtm         1/1     Running   0               26m
pod/emailservice-5b45ddff8c-wclpq            1/1     Running   0               26m
pod/cartservice-9849b489b-hr6q6              1/1     Running   0               26m
pod/recommendationservice-64f87465f-f657v    1/1     Running   0               26m
pod/loadgenerator-66f9dc4b65-wmzt2           1/1     Running   0               26m
pod/adservice-669f9767c9-lgplk               1/1     Running   0               26m

NAME                            TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
service/kubernetes              ClusterIP      10.43.0.1       <none>        443/TCP        5d6h
service/shippingservice         ClusterIP      10.43.212.108   <none>        50051/TCP      26m
service/productcatalogservice   ClusterIP      10.43.11.166    <none>        3550/TCP       26m
service/frontend                ClusterIP      10.43.52.146    <none>        80/TCP         26m
service/redis-cart              ClusterIP      10.43.29.131    <none>        6379/TCP       26m
service/frontend-external       LoadBalancer   10.43.190.247   <pending>     80:30932/TCP   26m
service/adservice               ClusterIP      10.43.238.175   <none>        9555/TCP       26m
service/cartservice             ClusterIP      10.43.254.122   <none>        7070/TCP       26m
service/currencyservice         ClusterIP      10.43.73.244    <none>        7000/TCP       26m
service/checkoutservice         ClusterIP      10.43.33.52     <none>        5050/TCP       26m
service/recommendationservice   ClusterIP      10.43.36.233    <none>        8080/TCP       26m
service/paymentservice          ClusterIP      10.43.180.121   <none>        50051/TCP      26m
service/emailservice            ClusterIP      10.43.137.47    <none>        5000/TCP       26m

NAME                                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/redis-cart              1/1     1            1           26m
deployment.apps/shippingservice         1/1     1            1           26m
deployment.apps/productcatalogservice   1/1     1            1           26m
deployment.apps/frontend                1/1     1            1           26m
deployment.apps/paymentservice          1/1     1            1           26m
deployment.apps/checkoutservice         1/1     1            1           26m
deployment.apps/currencyservice         1/1     1            1           26m
deployment.apps/emailservice            1/1     1            1           26m
deployment.apps/cartservice             1/1     1            1           26m
deployment.apps/recommendationservice   1/1     1            1           26m
deployment.apps/loadgenerator           1/1     1            1           26m
deployment.apps/adservice               0/1     1            0           26m

NAME                                               DESIRED   CURRENT   READY   AGE
replicaset.apps/adservice-669f9767c9               1         1         0       26m
replicaset.apps/redis-cart-696448dd5c              1         1         1       26m
replicaset.apps/shippingservice-6dc8984595         1         1         1       26m
replicaset.apps/productcatalogservice-76d9484687   1         1         1       26m
replicaset.apps/frontend-5f946c96fc                1         1         1       26m
replicaset.apps/paymentservice-6d65f6bff5          1         1         1       26m
replicaset.apps/checkoutservice-5f4f746c97         1         1         1       26m
replicaset.apps/currencyservice-795fc646df         1         1         1       26m
replicaset.apps/emailservice-5b45ddff8c            1         1         1       26m
replicaset.apps/cartservice-9849b489b              1         1         1       26m
replicaset.apps/recommendationservice-64f87465f    1         1         1       26m
replicaset.apps/loadgenerator-66f9dc4b65           1         1         1       26m
```
